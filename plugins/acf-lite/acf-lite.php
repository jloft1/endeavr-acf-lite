<?php
/**
 * Plugin Name: 	Advanced Custom Fields Activation (LITE)
 * Plugin URI: 	http://www.advancedcustomfields.com/
 * Description: 	The activation plugin for the lite version of the Advanced Custom Fields WordPress plugin
 * Version: 		4.0.3
 * Author: 		Elliot Condon
 * Author URI: 	http://www.elliotcondon.com/
 *
 * @package		acf-lite
 * @version		4.0.3
 * @since			4.0.0
 * @author		Elliot Condon
 * @copyright		Coppyright (c) 2013, Elliot Condon
 * @link			http://www.advancedcustomfields.com/
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

define( 'ACF_LITE' , true );