<?php
/**
 * Plugin Name: 	Advanced Custom Fields Activation (LITE)
 * Plugin URI: 	http://www.advancedcustomfields.com/
 * Description: 	The activation plugin for the lite version of the Advanced Custom Fields WordPress plugin
 * Version: 		4.0.3
 * Author: 		Elliot Condon
 * Author URI: 	http://www.elliotcondon.com/
 *
 * @package		acf-lite
 * @version		4.0.3
 * @since			4.0.0
 * @author		Elliot Condon
 * @copyright		Coppyright (c) 2013, Elliot Condon
 * @link			http://www.advancedcustomfields.com/
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

define( 'ACF_LITE' , true );
define( 'ENDVR_PLUGINS_DIR', WP_PLUGIN_DIR );
include_once( ENDVR_PLUGINS_DIR . '/advanced-custom-fields/acf.php' );
include_once( ENDVR_PLUGINS_DIR . '/acf-repeater/acf-repeater.php' );
include_once( ENDVR_PLUGINS_DIR . '/acf-flexible-content/acf-flexible-content.php' );
include_once( ENDVR_PLUGINS_DIR . '/acf-gallery/acf-gallery.php' );
include_once( ENDVR_PLUGINS_DIR . '/acf-options-page/acf-options-page.php' );